<?php
$url = $_SERVER['REQUEST_URI'];
$db = new mysqli('localhost', 'aqi', 'aqi', 'aqi');
if(preg_match('~/area/(\d+)\.json~', $url, $m))
{
	$areaid = intval($m[1]);
	$r = $db->query('SELECT `aqi_date`, `aqi_value` FROM `aqis2` WHERE `aqi_area` = ' . $areaid . ' ORDER BY `aqi_date` ASC');
	$data = $r->fetch_all(MYSQLI_ASSOC);
	$json = json_encode($data);
	if(file_exists('area'))
		file_put_contents('area/' . $areaid . '.json', $json);
	die($json);
}
if(preg_match('~/date/([0-9-]+)\.json~',$url, $m))
{
	$date = $m[1];
	$r = $db->query('SELECT `aqi_area`, `aqi_value` FROM `aqis2` WHERE `aqi_date` = \'' . $date . '\' ORDER BY `aqi_area` ASC');
	$data = $r->fetch_all(MYSQLI_ASSOC);
	$json = json_encode($data);
	if(file_exists('date'))
		file_put_contents('date/' . $date . '.json', $json);
	die($json);
}
if(preg_match('~/areas\.json~',$url, $m))
{
	$r = $db->query('SELECT * FROM `areas`');
	$data = $r->fetch_all(MYSQLI_ASSOC);
	$json = json_encode($data);
	file_put_contents('areas.json', $json);
	die($json);
}
if(strpos($url, 'bar.png') !== false)
{
	$img = imagecreatetruecolor(626, 80);
	$data = $db->query('SELECT SQL_CACHE `aqi_date` `date`, `aqi_value` div 25 `idx`, COUNT(*) `cnt`
		FROM `aqis2`
		GROUP BY `aqi_date`, (`aqi_value` div 25)
		ORDER BY `aqi_date` ASC
	');
	$slice = array_fill(0, 21, 0);
	$x = 0;
	$daycnt = 0;
	$olddate = '';
	$oldyear = 2001;

	$height = imagesy($img);
	$colors = ['55FF55','99FF55','CCFF55','FFFF55','FFCC55','FFAA55','FF7755','FF5555','DD5536','CC5526','AA443C','99334C','8C2238','8C1138','7E002A','6E0023','5E001A','4E0013','3E000A','2E0003','2E0003'];
	$black = imagecolorallocate($img, 0, 0, 0);
	$allocs = [];
	foreach($colors as $k => $c)
		$allocs[$k] = imagecolorallocate(
			$img,
			hexdec(substr($c, 0, 2)),
			hexdec(substr($c, 2, 2)),
			hexdec(substr($c, 4, 2)));

	foreach($data as $row)
	{
		extract($row);
		$year = intval($date);
		if($olddate != $date)
		{
			$daycnt++;
			if($daycnt == 8) // flush
			{
				drawslice($img, $x++, array_reverse($slice, true), $height, $allocs);
				$slice = array_fill(0, 21, 0);
				$daycnt -= 7;
			}
		}
		if($oldyear != $year)
		{
			imagedashedline($img, $x-1, 0, $x-1, $height, $black);
			$oldyear = $year;
		}
		$slice[$idx] += $cnt;
		$olddate = $date;
	}
	drawslice($img, $x++, array_reverse($slice, true), $height, $allocs);
	header('Content-Type: image/png');
	imagepng($img, 'bar.png');
	imagepng($img);
	die();
}
die('not match');

function drawslice($img, $x, $slice, $height, $allocs)
{
	$sum = array_sum($slice);
	$data = [];
	foreach($slice as $k => $d)
		$data[$k] = $d / $sum * $height;
	// draw
	$y = 0;
	foreach($data as $k => $d)
	{
		imageline($img, $x, intval($y), $x, intval($y+$d), $allocs[$k]);
		$y += $d;
	}

}