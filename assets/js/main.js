$('.choropleth').vectorMap({
	map: 'cn_mill_en',
	zoomOnScroll: 0,
	zoomMax: 2,
	backgroundColor:'transparent',
	fill:'',
	onRegionLabelShow: undefined
});

var color = d3.scale.quantize()
		.domain([0, 500])
		.range(d3.range(20).map(function(d) { return "q" + d; }));


$(function(){

function monthPath(t0) {
	var t1 = new Date(t0.getFullYear(), t0.getMonth() + 1, 0),
			d0 = +day(t0), w0 = +week(t0),
			d1 = +day(t1), w1 = +week(t1);
	return "M" + (w0 + 1) * cellSize + "," + d0 * cellSize
			+ "H" + w0 * cellSize + "V" + 7 * cellSize
			+ "H" + w1 * cellSize + "V" + (d1 + 1) * cellSize
			+ "H" + (w1 + 1) * cellSize + "V" + 0
			+ "H" + (w0 + 1) * cellSize + "Z";
}

width = 480;
height = 60;
cellSize = 8; // cell size

var day = d3.time.format("%w"),
		week = d3.time.format("%U"),
		format = d3.time.format("%Y-%m-%d");

var svg = d3.select(".heat").selectAll("svg")
		.data(d3.range(2001, 2013))
	.enter().append("svg")
		.attr("width", width)
		.attr("height", height)
		.attr("class", "year")
	.append("g")
		.attr("transform", "translate(" + ((width - cellSize * 53) / 2) + "," + (height - cellSize * 7 - 1) + ")");

svg.append("text")
		.attr("transform", "translate(-6," + cellSize * 3.5 + ")rotate(-90)")
		.style("text-anchor", "middle")
		.text(function(d) { return d; });

rect = svg.selectAll(".day")
		.data(function(d) { return d3.time.days(new Date(d, 0, 1), new Date(d + 1, 0, 1)); })
	.enter().append("rect")
		.attr("class", "day")
		.attr("width", cellSize)
		.attr("height", cellSize)
		.attr("x", function(d) { return week(d) * cellSize; })
		.attr("y", function(d) { return day(d) * cellSize; })
		.datum(format);

day_cnt = $('rect.day').each(function (i) { $(this).attr('day', i); }).length;

rect.append("title")
		.text(function(d) { return d; });

svg.selectAll(".month")
		.data(function(d) { return d3.time.months(new Date(d, 0, 1), new Date(d + 1, 0, 1)); })
	.enter().append("path")
		.attr("class", "month")
		.attr("d", monthPath);

choro = d3.select('.choropleth')
	.selectAll('path')
	.datum(function () {return $(this).attr('data-code').substr(3,2);})
	.append('title')
	;

d3.select('.main svg.legend')
	.selectAll('rect')
	.data(d3.range(0,20))
	.enter()
		.append('rect')
		.attr('x', function (d) {return d * cellSize * 2;})
		.attr('y', 0)
		.attr('width', cellSize * 2)
		.attr('height', cellSize * 2)
		.attr('class', function (d) { return 'q' + d; })
		.append('title')
			.text(function (d) { return d * 25 + "-" + (d+1) * 25;})
	;

var slider = $('.slider').slider({
	min: 0,
	max: day_cnt,
	value: 0,
	slide: function (event, ui) {
		$('.current-day').text(idx2date(ui.value));
		chart.dayidx(ui.value);
	},
	change: function (event, ui) {
		$('.current-day').text(idx2date(ui.value));
		chart();
	}
});

$('.year').click(function (e) {
	if(chart.areaid() == 0)
		return;
	var target = e.target || e.srcElement;
	var o = $(target);
	if(o.attr('class').indexOf('day') < 0) // Since `hasClass` in jQuery is broken
		return;
	var dayidx = o.attr('day');
	chart.dayidx(dayidx);
	// update related labels
	slider.slider('value', dayidx);
});

$('.choropleth svg').click(function (e) {
	var target = e.target || e.srcElement;
	var o = $(target);
	var areaid = o.prop('__data__');
	if(areaid == undefined)
		return;
	chart.areaid(areaid)();
	var old_active = $('.choropleth .active');
	if(old_active.length > 0)
		old_active[0].classList.remove('active');
	target.classList.add('active');
	$('.areaname').text(areas[areaid]);
});

$('.previous a').click(function () {chart.prev()(); slider.slider('value', chart.dayidx()); return false;});
    $('.next a').click(function () {chart.next()(); slider.slider('value', chart.dayidx()); return false;});

$('.playing-switch').on('switch-change', function (e, data) {
	if(data.value)
		playing = setInterval(function () {
			$('.next a').click();
		}, 500);
	else
		clearInterval(playing);
});

chart = aqiChart();

var gl = d3.select('svg.line')
					.append('g')
					.attr('class', 'line');
gl.append('line')
	.attr('class', 'h')
	.attr('x1', 0)
	.attr('x2', width)
	.attr('y1', 0)
	.attr('y2', 0)
	;
gl.append('line')
	.attr('class', 'v')
	.attr('y1', 0)
	.attr('y2', 800)
	.attr('x1', 0)
	.attr('x2', 0)
	;

areas = '';
d3.json('/areas.json', function (error, json) {
	areas = d3.nest()
		.key(function(d) { return d.aqi_area; })
		.rollup(function(d) { return d[0].name; })
		.map(json);
	slider.slider('value', 0);
	//$('.choropleth path[data-code=CN-31]')[0].classList.add('active');
});

$('.reference-line-switch').on('switch-change', function (e, data) {
	$('svg.line line').attr('class', data.value ? '' : 'hide');
});

});


;function idx2date(day) {
	return $('.day[day='+ day +']').prop('__data__');
}

;function aqiChart() {
	var _dayidx = 0;
	var choropleth_redraw = true;
	var _areaid = 31;
	var heatmap_redraw = true;

	var drawChoropleth = function (date) {
		d3.json('/date/'+date+'.json', function (error, json) {
			var data = d3.nest()
				.key(function(d) { return d.aqi_area; })
				.rollup(function(d) { return d[0].aqi_value; })
				.map(json);

			d3.select('.choropleth')
				.selectAll('path')
				.attr("class", function(d) {
					return color(data[d]) + (d == _areaid ? ' active' : '');
				})
				.select('title')
				.text(function(d) {
					return areas[d] + ": " + (data[d] ? data[d] : 'N/A');
				})
				;
			choropleth_redraw = false;
		});
	};

	var drawHeatmap = function (area) {
		d3.json('/area/'+area+'.json', function (error, json) {
			var data = d3.nest()
				.key(function(d) { return d.aqi_date; })
				.rollup(function(d) { return d[0].aqi_value; })
				.map(json);
			var the_day = idx2date(_dayidx);
			rect
				.attr("class", function(d) {
					return "day " + (data[d] ? color(data[d]) : 'q-na')
						 + (d == the_day ? ' active' : '');
				})
				.attr('aqi', function(d) {
					return data[d];
				})
				.select("title")
					.text(function(d) { return d + ": " + (data[d] ? data[d] : 'N/A'); });
			heatmap_redraw = false;

			// update today-aqi
			var day_block = $('.day.active');
			$('.today-aqi').text(day_block.attr('aqi'));
		});
	};

	var drawHeatmapLine = function (_dayidx) {
		var active_block = $('.heat .active');
		var year = active_block.parents('svg').prop('__data__');
		var offset = (year - 2001) * (height + 5);
		var x = 30 + parseInt(active_block.attr('x'));
		var y = 19 + parseInt(active_block.attr('y')) + offset;
		$('g.line line:eq(0)')
			.attr('y1', y)
			.attr('y2', y)
			;
		$('g.line line:eq(1)')
			.attr('x1', x)
			.attr('x2', x)
			;
	};

	var updateActiveRect = function (_dayidx) {
		var old_active = $('.heat .active');
		if(old_active.length > 0)
			old_active[0].classList.remove('active');
		var day_block = $('.day[day='+_dayidx+']');
		day_block[0].classList.add('active');
		$('.today-aqi').text(day_block.attr('aqi'));
	}

	function draw() {
		if(choropleth_redraw)
			drawChoropleth(idx2date(_dayidx));
		if(heatmap_redraw)
			drawHeatmap(_areaid);
		updateActiveRect(_dayidx);
		drawHeatmapLine(_dayidx);
	}

	draw.dayidx = function (v) { if(!arguments.length) return _dayidx; _dayidx = parseInt(v); choropleth_redraw = true; return draw; }
	draw.areaid = function (v) { if(!arguments.length) return _areaid; _areaid = parseInt(v); heatmap_redraw = true; return draw; }
	draw.prev = function () { if(_dayidx > 0)         { _dayidx--; choropleth_redraw = true; } return draw; }
	draw.next = function () { if(_dayidx < day_cnt-1) { _dayidx++; choropleth_redraw = true; } return draw; }
	return draw;
}


